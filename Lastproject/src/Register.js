import React from 'react'
import { Platform, View, Text, Image, ScrollView, TextInput, StyleSheet, Button,
    TouchableOpacity
 } from 'react-native'

const Register = () => {
    return (
        <ScrollView>
                <View style={styles.containerView}>
                    <Text style={styles.logintext}>Welcome</Text>
                    <Text style = {styles.logintext2}>Sign up to continue</Text>
                    <View style={styles.forminput}>
                        <Text style={styles.formtext}>Name</Text>
                        <TextInput style={styles.input} />
                    </View>
                    <View style={styles.forminput}>
                        <Text style={styles.formtext}>Email</Text>
                        <TextInput style={styles.input} textContentType={'emailAddress'} />
                    </View>
                    <View style={styles.forminput}>
                        <Text style={styles.formtext} >Phone Number </Text>
                        <TextInput style={styles.input} secureTextEntry={true} />
                    </View>
                    <View style={styles.forminput}>
                        <Text style={styles.formtext}>Password</Text>
                        <TextInput style={styles.input} secureTextEntry={true} />
                    </View>
                    <View style={styles.kotaklogin}>
                        <TouchableOpacity style={styles.btreg} >
                            <Text style={styles.textbt}> Sign Up </Text>y
                        </TouchableOpacity>
                    </View>
                    <View style = {styles.textsign}>
                        <Text style = {styles.already}>Already have an account ?  </Text>
                        <Button onPress={()=>navigation.navigate('Home')}  title="Sign in"/>
                    </View>
                </View>
            </ScrollView>

    )
}

export default Register

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    containerView: {
        backgroundColor: '#fff',
        marginTop: 63,
        // alignItems: 'center',
        flex: 1
    },
    logintext: {
        fontSize: 24,
        color: '#0c0423',
        textAlign : 'left',
        marginLeft : 10,
        marginVertical: 20
        
    },
    logintext2 : {
        fontSize : 15,
        color : '#4d4d4d',
        marginLeft : 10,
        marginBottom : 50,

    },
    formtext: {
        color: '#003366',
    },
    atautext: {
        fontSize: 24,
        color: '#3EC6FF',
        textAlign: "center"
    },
    forminput: {
        marginHorizontal: 30,
        marginVertical: 5,
        alignContent: 'center',
        width: 294
    },
    input: {
        height: 40,
    },
    vbutton: {
        marginHorizontal: 90,
        borderRadius: 10,
        marginVertical: 10,
    },
    btlogin: {
        alignItems: "center",
        backgroundColor: "#3EC6FF",
        padding: 10,
        borderRadius: 16,
        marginHorizontal: 30,
        marginTop: 10,
        width: 140
    },
    btreg: {
        alignItems: "center",
        backgroundColor: "#f77866",
        textDecorationColor: '#000',
        padding: 10,
        borderRadius: 10,
        marginHorizontal: 30,
        marginBottom: 10,
        width: 300
    },
    textbt: {
        color: 'white',
        textAlign: "center",
        fontSize: 15,
        fontWeight: "bold",
    },
    kotaklogin: {
        marginTop: 20,
        alignItems: 'center'
    },
    textsign : {
        alignItems : 'center',
        flexDirection : 'row',
        justifyContent : 'center'
    },
    already : {
        color : '#4d4d4d',
        fontSize : 12,
    },
    halamansign : {
        color : '#f77866',
        fontSize : 12,
    }

})